import openpyxl
from openpyxl import Workbook
import re

def extract_error_messages(file_path):
    with open(file_path,'r') as file:
        log_content = file.read()
    lines = log_content.split("\n")
    error_message=[]
    for index,line in enumerate(lines):
        if"| ERROR |" in line:
            message = line.split("|")[4].strip()
        error_message.append(message)
    return error_message

def calculate_error_precise(error_message, out_file_name):
    total_errors = len(error_messages)
    print("total error "+str(total_errors))
    error_counts = {}
    for message in error_messages:
        if message in error_counts:
            error_counts[message] += 1
        else:
            error_counts[message] = 1
    error_statistics = {k: {"count": v, "percentage": (v / total_errors * 100)} for k, v in error_counts.items()}
    wb = Workbook()
    ws = wb.active
    ws.append(["Error Message","Count","Percentage"])
    for error_msg,count in error_counts.items():
        percentage = (count/total_errors) * 100
        ws.append([error_msg,count,percentage])

    wb.save(str(out_put_file_name)+".xlsx")
    print("total error type : "+ str(len(error_counts)))
    return error_statistics


def calculate_error_statistics(error_messages,out_put_file_name):
    total_errors = len(error_messages)
    print("total error "+str(total_errors))
    error_counts = {}
    for message in error_messages:
        if "UC inconnue" in message:
            if "UC inconnue" in error_counts:
                error_counts["UC inconnue"] += 1
            else:
                error_counts["UC inconnue"] = 1
        elif "Produit inconnu" in message:
            if "Produit inconnu" in error_counts:
                error_counts["Produit inconnu"] += 1
            else:
                error_counts["Produit inconnu"] = 1
        elif "Top level error" in message :
            if "Top level error" in error_counts:
                error_counts["Top level error"] += 1
            else:
                error_counts["Top level error"] = 1
        elif message =="" or message == "null" or message == "Fault occurred!" :
            if "inconnu" in error_counts:
                error_counts["inconnu"] += 1
            else: 
                error_counts["inconnu"] =1
        elif re.match("Utilisateur(.*)inexistant dans le Ldap",message)!=None:
            if "Utilisateur inexistant dans le Ldap" in error_counts:
                error_counts["Utilisateur inexistant dans le Ldap"]+=1
            else:
                error_counts["Utilisateur inexistant dans le Ldap"] = 1
        else:
            if message in error_counts:
                error_counts[message] += 1
            else:
                error_counts[message] = 1
    error_statistics = {k: {"count": v, "percentage": (v / total_errors * 100)} for k, v in error_counts.items()}
    wb = Workbook()
    ws = wb.active
    ws.append(["Error Message","Count","Percentage"])
    for error_msg,count in error_counts.items():
        percentage = (count/total_errors) * 100
        ws.append([error_msg,count,percentage])

    wb.save(str(out_put_file_name)+".xlsx")
    print("total error type : "+ str(len(error_counts)))
    return error_statistics

def print_error_statistics(error_statistics):
    print(f"{'Error Message':<50} {'Count':<10} {'Percentage':<10}")
    print('-' * 70)
    for error_message, stats in error_statistics.items():
        print(f"{error_message:<50} {stats['count']:<10} {stats['percentage']:.2f}%")
    print("\n")


if __name__ == "__main__":
    #\\MV_L130FIC0_PR\logs_web\projets\icone6\icone6Server02\logs\icone6-troubleshooting.log
    #\\MV_L130FIC0_PR\logs_web\projets\icone6\icone6Server01\logs\icone6-troubleshooting.log
    log_file_path=input("please entre path to file: ")
    out_put_file_name=input("please entre the name of file out: ")
    error_messages = extract_error_messages(log_file_path)
    error_statistics = calculate_error_precise(error_messages,out_put_file_name)
    print_error_statistics(error_statistics)

